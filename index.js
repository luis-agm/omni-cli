#!/usr/bin/env node
const fig = require( 'figlet' )
const chlk = require( 'chalk' )
const { version, name } = require( './package.json' )
const tryCommand = require( './commands/try' )

console.log( chlk.yellow( fig.textSync( 'API-TESTER', { font: 'epic' } ) ), '\n' )

// eslint-disable-next-line no-unused-vars
const wat = require( 'yargs' )
    .usage( `Usage: ${name} <command> [options]` )
    .command( tryCommand )
    .demandCommand()
    .version( version )
    .help( 'h' )
    .alias( 'h', 'help' ).argv
