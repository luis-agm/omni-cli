const ax = require( 'axios' )
const chlk = require( 'chalk' )
const { name } = require( '../package.json' )

const command = 'try'
const describe = 'Make subsequent requests to an API'

const builder = yargs => yargs
    .usage( `Usage: ${name} try [options]` )
    .describe( 'jojotico', 'Dumb option that does nothing but sounds funny out loud.' )
    .option( 'u', { description: 'Set URL to test', default: 'http://api.internal.ml.com/users/me/bookmarks?caller.id=182723901' } )
    .alias( 'u', 'url' )
    .nargs( 'u', 1 )
    .option( 'd', { description: 'Print data received' } )
    .alias( 'd', 'data' )
    .nargs( 'd', 0 )
    .option( 't', { description: 'Time interval in ms', default: 1000 } )
    .alias( 't', 'time' )
    .nargs( 't', 1 )
    .example( `${name} try -u www.google.com`, '-- Make several requests to Google' )

const handler = argv => {
    let TOP = 0

    console.log( `Starting requests to ${argv.u}, with ${argv.t}ms interval.` )
    console.log( 'Press Ctrl+C to stop.\n' )

    setInterval( () => {
        const time = Date.now()
        const taken = wot => Date.now() - wot

        const logValue = ( label, ...text ) => {
            console.log( label, TOP > 1000 ? chlk.red( ...text ) : chlk.yellow( ...text ) )
        }

        ax.get( argv.u || argv.url )
            .then( ( { data } ) => {
                const takenNow = taken( time )
                if ( takenNow > TOP ) TOP = takenNow
                logValue( 'TOP:', TOP )
                logValue( `SUCCESS: `, `${takenNow}ms` )
            } )
            .catch( () => {
                logValue( 'TOP:', TOP )
                logValue( `FAIL:`, `${taken( time )}ms \n\n` )
            } )
    }, argv.t )
}

module.exports = {
    command,
    builder,
    describe,
    handler,
}
